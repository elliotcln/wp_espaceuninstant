<?php

get_header();

$locale = explode('_', get_locale());
$posts = get_posts(array(
    'post_type' => 'projects',
    'numberposts' => -1,
    'orderby' => 'menu_order',
    'lang' => $locale[0]
));

?>
<main class="container" role="main">
    <div class="page-content projects-list pb-20">
        <?php if ($posts) : foreach ($posts as $post) : setup_postdata($post); ?>
                <div class="project md:flex place-items-center">
                    <figure class="project-thumb relative md:w-8/12 boxed lg:h-500" data-target="reveal">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('medium_large', ['class' => 'h-full w-full object-cover']); ?>
                        </a>
                        <div class=" z-50 hidden md:block project-tags absolute transform font-serif text-pink text-sm origin-bottom-center">
                            <?php
                            $tags = get_the_tags();
                            if ($tags) {
                                foreach ($tags as $tag) {
                                    echo '<span>' . $tag->name . '</span>';
                                }
                            }
                            ?>
                        </div>
                    </figure>
                    <div data-target="reveal" class="project-desc relative md:w-4/12 p-4 rounded bg-white shadow-2xl md:shadow-none mx-auto max-w-md -mt-10 relative">
                        <a href="<?php the_permalink(); ?>" class="page-title"><?php the_title(); ?></a>
                        <p class="text-base text-gray-600"><?php echo get_the_excerpt(); ?></p>
                        <a href="<?php the_permalink(); ?>" class="hidden lg:flex read-more place-items-center">
                            <?php pll_e('read-more'); ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/read-more-arrow.svg" alt="">
                        </a>
                    </div>
                </div>
            <?php endforeach;
        else :
            $no_data = 'No projects in this language. Try the other one ...';
            if ($locale === 'fr') $no_data = "Aucun résultats dans cette langue. Essayez en Anglais ...";

            ?>
            <p class="text-xl pt-6 text-center text-gray-600"><?php echo $no_data; ?></p>
        <?php
        endif;
        wp_reset_postdata(); ?>
    </div>
</main>

<?php

get_footer();
