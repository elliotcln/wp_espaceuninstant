<article <?php post_class(); ?> id="project-<?php the_ID(); ?>">
    <div class="container">
        <div class="md:flex md:place-items-center">
            <figure class="md:w-8/12 md:order-2 boxed lg:h-500">
                <?php the_post_thumbnail('medium_large', ['class' => 'h-full w-full object-cover']); ?>
            </figure>
            <div class="md:w-4/12 p-4 rounded bg-white shadow-2xl md:shadow-none mx-auto max-w-md -mt-10 relative md:mr-6">
                <h2 class="project-title text-5xl uppercase"><?php the_title(); ?></h2>
                <?php if(get_field('client')): ?>
                    <div class="project-ref">
                        <div class="font-serif font-semibold">Client</div>
                        <p class="text-base text-gray-600"><?= get_field('client'); ?></p>
                    </div>
                <?php endif; ?>
                <?php if(get_field('localisation')): ?>
                    <div class="project-ref">
                        <div class="font-serif font-semibold">Localisation</div>
                        <p class="text-base text-gray-600"><?= get_field('localisation'); ?></p>
                    </div>
                <?php endif; ?>
                <?php if(get_field('surface')): ?>
                    <div class="project-ref">
                        <div class="font-serif font-semibold">Surface</div>
                        <p class="text-base text-gray-600"><?= get_field('surface'); ?> M<sup>2</sup></p>
                    </div>
                <?php endif; ?>
                <?php if(get_field('date_de_livraison')): ?>
                    <div class="project-ref">
                        <div class="font-serif font-semibold"><?php pll_e('delivery_date'); ?></div>
                        <p class="text-base text-gray-600"><?= get_field('date_de_livraison'); ?></p>
                    </div>
                <?php endif; ?>
                <?php if(get_field('mission')): ?>
                    <div class="project-ref">
                        <div class="font-serif font-semibold">Mission</div>
                        <p class="text-base text-gray-600"><?= get_field('mission'); ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <!-- conttent -->
        <div class="project-content mt-6 md:mt-10 mx-auto max-w-lg md:max-w-3xl">
            <?php the_content(); ?>
        </div>
    </div>
</article>