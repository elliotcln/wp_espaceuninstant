<main role="main">
    <div class="container">
        <?php
        // contact
        if (is_page('contact') || is_page('contact-en')) : ?>
            <div class="page-content py:6 md:py-10">
                <div class="grid md:grid-cols-5 gap-4 md:gap-24">
                    <div class="md:col-span-3">
                        <!-- prestations -->
                        <h3 class="text-2xl font-medium font-serif">Mes préstations</h3>
                        <ul class="text-gray-800 py-4">
                            <li class="p-2 border-b">1er RDV et conseil (prise de connaissance dossier, sans rapport)</li>
                            <li class="p-2 border-b">Etude de projet préliminaire</li>
                            <li class="p-2 border-b">Etude de projet détaillé</li>
                            <li class="p-2 text-sm text-gray-500">Dispensé d’immatriculation au registre du commerce et des sociétés (RCS) et au répertoire des métiers (RM).</li>
                        </ul>
                        <p class="text-gray-600 text-sm">L’étude de projet préliminaire inclus 2 rendez-vous clients, rédaction de rapport, création et développement d’un concept, réalisation d’un plan de l’existant (si non-existant au préalable) et des plans de propositions d’aménagement, planche d’ambiance et une visualisation 3D.</p>
                        <p class="text-gray-600 text-sm mt-2">L’étude de projet détaillé inclus dans un premier temps, toutes les étapes de l’étude préliminaire. Cette étude contient en complément, les plans ajustés et définitifs du projet, les plans techniques nécessaires à la réalisation des travaux (détails techniques, côtes, etc.), choix des matériaux, vues 3D finales, listing des corps de métier nécessaires.</p>

                        <!-- prestations pro -->
                        <h3 class="text-2xl font-medium font-serif mt-6">Préstations détaillées pour profesionnel</h3>
                        <ul class="text-gray-800 py-4">
                            <li class="p-2 border-b">Réalisation plan sol 2D</li>
                            <li class="p-2 border-b">Réalisation de détails techniques 2D</li>
                            <li class="p-2 border-b">Graphisme présentation (sur InDesign)</li>
                            <li class="p-2 border-b">Création de l’espace en 3D</li>
                            <li class="p-2 border-b">Rendu 3D VR intérieur</li>
                            <li class="p-2 border-b">Rendu 3D VR extèrieur</li>
                            <li class="p-2 text-sm text-gray-500">Dispensé d’immatriculation au registre du commerce et des sociétés (RCS) et au répertoire des métiers (RM).</li>
                        </ul>
                        <p class="text-gray-600 text-sm">Chaque projet fera l’objet d’un devis forfaitaire.</br>
                            Toutes demandes supplémentaires seront facturées à hauteur des tarifs ci-dessus.</br>
                            Tous frais de déplacement seront au frais du client.</p>
                    </div>
                    <div class="md:col-span-2">
                        <?php the_content(); ?>
                        <div class="md:flex flex-col md:place-items-center my-6 max-w-2xl">
                            <a class="w-full btn btn--gray__outline mb-4" href="mailto:<?php echo get_option('admin_email'); ?>">
                                <img src="/wp-content/themes/espaceuninstant/assets/images/mail-icon.svg" alt="">
                                <span class="ml-4"><?php echo get_option('admin_email'); ?></span>
                            </a>
                            <a class="w-full btn btn--gray__outline" href="tel:<?php echo get_option('phone_number'); ?>">
                                <img src="/wp-content/themes/espaceuninstant/assets/images/tel-icon.svg" alt="">
                                <span class="ml-4"><?php echo get_option('phone_number'); ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        // about
        elseif (is_page('about') || is_page('a-propos')) : ?>
            <figure class="alignfull h-64 lg:h-350 overflow-hidden bg-cover bg-center" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                <!-- <?php the_post_thumbnail('full', ['class' => 'w-full']); ?> -->
            </figure>
            <div class="grid grid-col md:grid-cols-2 lg:grid-cols-3 gap-6 py-6 md:py-20">
                <div class="md:col-span-1 flex flex-col items-center -mt-32">
                    <figure class="boxed boxed--right">
                        <?php if(get_field('photo')) : $p = get_field('photo'); ?>
                            <img src="<?= $p['url']; ?>" alt="<?= $p['alt']; ?>" loading="lazy"/>
                        <?php endif; ?>
                    </figure>
                    <div class="text-center mt-6">
                        <?php
                        $cv = wp_get_attachment_url(239);
                        ?>
                        <a href="<?php echo $cv; ?>" download="cv-portfolio-lilia-longe" class="btn btn--pink">
                            <?php pll_e('resume'); ?>
                            <img class="ml-4" src="<?php echo get_template_directory_uri(); ?>/assets/images/button-arrow.svg" alt="">
                        </a>
                    </div>
                </div>
                <div class="md:col-span-1 lg:col-span-2">
                    <div class="max-w-md mx-auto">
                        <h3 class="page-title">Lilia Longé</h3>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        <?php
        elseif (is_page('approche') || is_page('approach')) : ?>
            <!-- <figure class="boxed boxed--right">
                <?php the_post_thumbnail('full', ['class' => 'w-full lg:h-500 object-cover']); ?>
            </figure>
            <div class="max-w-md md:max-w-xl mx-auto lg:ml-auto lg:mr-4 transform -translate-y-32 p-4 rounded bg-white">
                <h2 class="page-title"><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div> -->
            <div class="text-xl py-6 lg:py-10">
                <?php the_content(); ?>
            </div>
        <?php
        endif;
        ?>
    </div>
</main>