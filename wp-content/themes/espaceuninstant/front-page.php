<?php

get_header();
?>

<main class="container md:pt-10" role="main">
    <div class="text-center">
        <h2 class="text-5xl inline-block page-title">Mood</h2>
        <div class="content my-10">
            <?php
            $post = get_post(62);

            if (has_block('paragraph')) {
                $post_p = parse_blocks($post->post_content);
                $p1 = $post_p[0]['innerHTML'];

                echo $p1;
            }
            ?>
            <div class="masonry-grid mx-auto py-10">
                <?php

                if (has_block('gallery')) {
                    $post_blocks = parse_blocks($post->post_content);
                    $ids = $post_blocks[2]['attrs']['ids'];

                    foreach ($ids as $id) {
                        $image = wp_get_attachment_image_url($id, 'medium_large');
                ?>
                        <div class="grid-item" data-target="reveal">
                            <img class="rounded w-full" loading="lazy" src="<?php echo $image; ?>" alt="">
                        </div>
                <?php
                    }
                }
                ?>
            </div>
            <?php
            if (has_block('paragraph')) {
                $post_p = parse_blocks($post->post_content);
                $p2 = $post_p[4]['innerContent'][0];
                echo $p2;
            }
            ?>
        </div>
    </div>
    <?php

    ?>
</main>

<?php
get_footer();
