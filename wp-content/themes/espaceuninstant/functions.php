<?php

/**
 * Add theme scripts & styles
 */
function add_theme_scripts()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_script('masonry', get_template_directory_uri() . '/assets/js/masonry.js', array(), '1.0.0', true);
    wp_enqueue_script('scrollreveal', 'https://unpkg.com/scrollreveal', array(), '1.0.0', true);
    wp_enqueue_script('app', get_template_directory_uri() . '/assets/js/app.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

/**
 * Define theme supports
 */
function uninstant_theme_support()
{

    // Add custom logo support
    add_theme_support('custom-logo', [
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array('site-title', 'site-description'),
        'unlink-homepage-logo' => true
    ]);

    // Add support for full and wide align images.
    add_theme_support('align-wide');
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('menus');
}

add_action('after_setup_theme', 'uninstant_theme_support');

/**
 * Remove items in admin menu
 */
function wpdocs_remove_menus()
{
    // remove posts & comments
    remove_menu_page('edit.php');
    // remove_menu_page('edit.php?post_type=acf-field-group');
    remove_menu_page('edit-comments.php');
    // remove_menu_page('mlang');
}

add_action('admin_menu', 'wpdocs_remove_menus');

/**
 * Register
 */
function register()
{

    // Projects post type
    register_post_type('projects', array(
        'labels' => [
            'name' => __('Projets'),
            'singular_name' => __('Projet'),
            'all_items' => __('Tous les projets'),
            'add_new' => __('Ajouter un projet')
        ],
        'taxonomies' => array('post_tag'),
        'menu_icon' => 'dashicons-art',
        'public' => true,
        'has_archives' => true,
        'rewrite' => array('slug' => 'projet'),
        'show_in_rest' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'supports' => ['title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes']
    ));

    register_nav_menus(array(
        'main-menu' => __('Main Menu'),
        'social-menu' => __('Social Menu'),
    ));

    pll_register_string('espace-un-instant', 'mood-of-day');
    pll_register_string('espace-un-instant', 'socials');
    pll_register_string('espace-un-instant', 'cta-contact');
    pll_register_string('espace-un-instant', 'contact');

    pll_register_string('project', 'delivery_date');
    pll_register_string('project', 'read-more');
    pll_register_string('about', 'resume');
}

add_action('init', 'register');

function set_admin_init()
{
    register_setting('general', 'phone_number', array(
        'type' => 'string',
        'default' => '06 ......'
    ));
    add_settings_field(
        'phone_number',
        'Numero de telephone',
        'callback_phone_number',
        'general'
    );
}
add_action('admin_init', 'set_admin_init');
function callback_phone_number()
{
    $phone_number = get_option('phone_number', '');
    echo "<input type='text' name='phone_number' id='phone_number' value='" . $phone_number . "'/>";
}

/**
 * Rewrite permalinks when changes theme
 */
function theme_prefix_rewrite_flush()
{
    flush_rewrite_rules();
}
add_action('after_switch_theme', 'theme_prefix_rewrite_flush');
