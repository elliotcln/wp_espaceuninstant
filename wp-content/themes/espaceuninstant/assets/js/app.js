const loading = document.querySelector('.site-loading')
loading.classList.remove('hidden')
if (getCookie('has-loaded')) {
    loading.classList.add('hidden')
} else {
    setTimeout(() => {
        setCookie('has-loaded', true, 1)
        loading.classList.add('is-out')
        loading.classList.add('hidden')
    }, 2000);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

window.addEventListener('load', event => {

    var msnry = new Masonry('.masonry-grid', {
        itemSelector: '.grid-item',
        gutter: 10,
        isFitWidth: true
    });

    const reveal_opts = {
        delay: 300,
        duration: 1500,
        distance: '20px'
    }
    const project_content = document.querySelectorAll('.project-content *')
    const reveals = document.querySelectorAll('[data-target="reveal"], .js-reveal')
    ScrollReveal().reveal(project_content, reveal_opts);
    ScrollReveal().reveal(reveals, reveal_opts);

    // toggle nav
    const nav_button = document.querySelector('.toggle-nav')
    const main_nav = document.querySelector('.main-nav')
    nav_button.addEventListener('click', () => {
        main_nav.classList.toggle('hidden')
    })

})