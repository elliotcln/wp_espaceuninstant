const { task, series, parallel, src, dest, watch } = require('gulp')
const $ = require('gulp-load-plugins')()

/**
 * compile sass files
 */
task('compile:sass', () => {
    return src('./style.scss')
        .pipe($.sass({
            includePaths: ['node_modules']
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({
            cascade: false
        }))
        .pipe($.postcss())
        .pipe(dest('.'))
})

task('purge:css', () => {
    return src('./style.css')
        .pipe($.purgecss({
            content: ['**/*.php', '**/*.scss'],
            safelist: {
                standard: [/^[smlx][smdg]:[a-z]{2}-\d*$/, /^[a-z]{2}-\d*/]
            },
            defaultExtractor: content =>
                content.match(/[\w-/:]+(?<!:)/g) || []
        }))
        .pipe(dest('.'))
})

/**
 * watch function (compile Sass & JS files)
 */
exports.default = () => {
    watch(['./style.scss', './sass/**/*.scss'],
        series('compile:sass', 'purge:css'))
}