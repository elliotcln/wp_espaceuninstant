module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Source Sans Pro', 'sans-serif'],
        serif: ['Source Serif Pro', 'serif'],
      },
      colors: {
        pink: '#cb7375'
      },
      height: {
        '350': '350px',
        '500': '500px'
      }
    },
  },
  variants: {},
  plugins: [],
}
