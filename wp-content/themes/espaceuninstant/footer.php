<footer>
    <div class="z-50 opacity-50 background-logo fixed top-0 left-0 w-screen h-screen flex place-items-center justify-center pointer-events-none">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-bg.png" alt="<?php bloginfo('description'); ?>">
    </div>
    <?php
        // $name = trim('chihiro011');
        // $args = array(
        //     'post_type' => 'attachment',
        //     // 'name' => $name,
        //     'post_per_page' => -1
        // );
        // $attachments = new WP_Query($args);
        // $images = array();
        // foreach( $attachments->posts as $image) {
        //     $images[] = $image->guid;
        // }

        $footer_bg = wp_get_attachment_image_url(177, 'full');
    ?>
    <div class="cta-contact py-10 bg-gray-500 h-64 flex place-items-center relative">
        <div class="absolute top-0 left-0 w-full h-full bg-cover bg-center opacity-25 z-0" style="background-image: url(<?php echo $footer_bg; ?>);"></div>
        <div class="container flex flex-col items-center relative">
            <p class="text-3xl text-center text-white font-serif max-w-sm mx-auto">
                <?php pll_e("cta-contact"); ?>
            </p>
            <?php 
                $locale = explode('_', get_locale());
                if($locale[0] === 'fr') {
                    $contact = 'contact';
                } else {
                    $contact = 'contact-en';
                }

                $page = get_page_by_path($contact);
            ?>
            <a href="<?php echo get_page_link($page->ID); ?>" class=" mt-6 btn btn--pink">
                <?php pll_e('contact'); ?>
                <img class="ml-4" src="<?php echo get_template_directory_uri(); ?>/assets/images/button-arrow.svg" alt="">
            </a>
        </div>
    </div>
    <div class="container py-10 md:flex md:place-items-start">
        <div class="flex place-items-center">
            <img class="w-32" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-footer.png" alt="">
            <div class="ml-4 flex flex-col">
                <span class="font-semibold font-serif text-xl">L’espace d’un instant</span>
                <div class="text-gray-600 leading-tight">
                    <?php bloginfo('description'); ?><br />
                    <?php echo get_option('admin_email'); ?><br />
                    <?php echo get_option('phone_number'); ?>
                </div>
            </div>
        </div>
        <div class="socials md:ml-auto mt-6 md:mt-3 flex flex-col items-center md:items-start">
            <span class="font-semibold font-serif text-xl"><?php pll_e('socials'); ?></span>
            <?php wp_nav_menu(array(
                'menu' => 'social-menu',
                'menu_class' => 'social-menu'
            )); ?>
        </div>
    </div>
    <div class="container text-center font-semibold text-xl font-serif mb-6">
        <p>&copy <?php echo date("Y") . ' - ' . (date("Y") + 1); ?></p>
    </div>
</footer>

<?php wp_footer(); ?>
</body>

</html>