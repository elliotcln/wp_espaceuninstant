<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta <?php bloginfo('charset'); ?>>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title(); ?> - <?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body>
    <div class="site-loading hidden">
        <div class="md:flex place-items-center">
            <img class="w-32" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-footer.png" alt="">
            <h2 class="text-4xl leading-tight md:ml-4">L'espace d'un instant
                <small class="block text-gray-600"><?php bloginfo('description'); ?></small>
            </h2>
        </div>
    </div>
    <h1 class="sr-only"><?php bloginfo('description'); ?></h1>
    <header class="main-navigation relative z-40" role="header">
        <div class="container flex flex-col md:flex-row md:place-items-center">
            <div class="brand flex place-items-center relative py-4 md:py-6">
                <a href="<?php echo site_url(); ?>" class="md:hidden flex place-items-center">
                    <img class="w-16" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-footer.png" alt="">
                </a>
                <a href="<?php echo site_url(); ?>" class="font-serif text-2xl leading-tight hidden md:block">L'espace d'un instant
                    <small class="block text-gray-500">Architecte d'intérieur & designer</small>
                </a>

                <button class="toggle-nav md:hidden absolute right-0 w-16 h-16 flex place-items-center justify-center">
                    <img class="w-8" src="<?php echo get_template_directory_uri(); ?>/assets/images/menu-bars.svg" alt="Toggle Navbar">
                </button>
            </div>
            <div class="main-nav hidden flex-shrink-0 md:flex md:place-items-center md:ml-auto mb-6 md:mb-0">
                <?php wp_nav_menu(array(
                    'menu' => 'main-menu',
                    'container' => 'nav',
                    'menu_class' => 'md:flex flex-shrink-0',
                ));

                // pll_the_languages(array(
                //     'dropdown' => 1,
                //     'display_names_as' => 'slug'
                // ));
                ?>
            </div>
        </div>
    </header>